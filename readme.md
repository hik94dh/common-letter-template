## Start

* Install dependencies by run `yarn` or `npm i`
* Run `gulp`

### Deploy

* Add repository to file `gulpfile.js`
* Add task name to file `./src/settings.js`
* Run `yarn d` or `npm run d` for first deploy
* Run `yarn u` or `npm run u` for update deploy

### Project structure

* `./src` - root project folder.
* `./src/data.pug` - data file.
* `./src/settings.js` - settings file.
* `./src/i/` - folder for all images.
* `./src/style/style.scss` - styles file.
* `./src/pug/_js.pug` - file for helpers scripts.
* `./src/pug/_layout.pug` - wrapper for `index.pug`.
* `./src/pug/_mixins.pug` - common mixins.
* `./src/pug/index.pug` - entry point.
