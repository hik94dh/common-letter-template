const browserSync = require('browser-sync');
const dirSync = require('gulp-directory-sync');
const git = require('gulp-git');
const gulp = require('gulp');
const gulpPug = require('gulp-pug');
const htmlmin = require('gulp-htmlmin');
const inlineCss = require('gulp-inline-css');
const imagemin = require('gulp-imagemin');
const plumber = require('gulp-plumber');
const pugLinter = require('gulp-pug-linter');
const sass = require('gulp-sass');
const rimraf = require('rimraf');
const zip = require('gulp-zip');

const settings = require('./src/settings.js');

const bbRepo  = 'hik94dh.bitbucket.io';
const publicLink = `https://${bbRepo}/${settings.taskname}/index.html`;
const bbUrl = `git@bitbucket.org:hik94dh/${bbRepo}.git`;
const inputDir = 'src/';
const outputDir = 'build/';

const config = {
    server: {
        baseDir: outputDir,
    },
    port: 9000,
    open: true,
    reloadDelay: 1000,
};

gulp.task('style', () =>
   gulp
       .src(`${inputDir}style/*.scss`)
       .pipe(plumber())
       .pipe(sass())
       .pipe(gulp.dest(`${inputDir}style`))
       .pipe(browserSync.stream())
);

gulp.task('lint', () =>
    gulp
        .src(inputDir)
        .pipe(pugLinter())
        .pipe(pugLinter.reporter())
);

gulp.task('pug', ['lint', 'style'], () => {
    return gulp
        .src([`!${inputDir}pug/_*.pug`, `${inputDir}pug/*.pug`])
        .pipe(plumber())
        .pipe(gulpPug({pretty: true}))
        .pipe(inlineCss(
            {
                preserveMediaQueries: true,
                applyStyleTags: false,
                removeStyleTags: false
            }))
        .pipe(gulp.dest(outputDir))
        .pipe(browserSync.stream())
});

gulp.task('img', () =>
    gulp.src('')
        .pipe(
            dirSync('src/i/', 'build/i/', { printSummary: true }
            ))
        .pipe(browserSync.stream())
);

gulp.task('zip', () => {
    gulp
        .src(`${outputDir}**/*`)
        .pipe(zip(`${settings.taskname}.zip`))
        .pipe(gulp.dest(''))
});

gulp.task('clean', cb => {
    rimraf(outputDir, cb);
});

gulp.task('build', ['pug', 'img']);

gulp.task('webserver', () => {
    browserSync(config);
});

gulp.task('watch', function() {
    gulp.watch(`${inputDir}**/*.pug`, ['pug']);
    gulp.watch(`${inputDir}style/**/*.scss`, ['pug']);
    gulp.watch(`${inputDir}i/**/*`, ['img']);
});

gulp.task('default', [
    'pug',
    'style',
    'img',
    'watch',
    'webserver',
]);

// === Deploy ===

gulp.task('clone', () => {
    git.clone(bbUrl, err => {
        if (err) throw err;
    });
});

gulp.task('addFolder', () => {
    gulp
        .src(`${outputDir}**/*.*`)
        .pipe(gulp.dest(`${bbRepo}/${settings.taskname}`));
});

gulp.task('replaceFolder', cb => {
    rimraf(`${bbRepo}/${settings.taskname}`, cb);
});

gulp.task('updateFolder', ['replaceFolder'], () => {
    gulp
        .src(`${outputDir}**/*.*`)
        .pipe(gulp.dest(`${bbRepo}/${settings.taskname}`));
});

gulp.task('addCommit', () => {
    process.chdir(bbRepo);
    gulp.src(`${settings.taskname}/*`)
        .pipe(git.add())
        .pipe(git.commit(`Add ${settings.taskname} letter`));
});

gulp.task('updateCommit', () => {
    process.chdir(bbRepo);
    gulp.src(`${settings.taskname}/*`)
        .pipe(git.add())
        .pipe(git.commit(`Update ${settings.taskname} letter`));
});

gulp.task('push', () => {
    process.chdir(bbRepo);
    git.push('origin', 'master', err => {
        if (err) throw err;
    });
});

gulp.task('removeRepo', cb => {
    rimraf(bbRepo, cb);
    console.log(`****************** ${publicLink} ******************`)
});
